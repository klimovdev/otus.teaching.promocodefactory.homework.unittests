﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Infrastructure;
using Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Builders;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Linq;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;
        private readonly Mock<ICurrentDateTimeProvider> _currentDateTimeProviderMock;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _currentDateTimeProviderMock = fixture.Freeze<Mock<ICurrentDateTimeProvider>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFoundRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = null;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, null);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var partner = PartnerBuilder.CreateBasePartner();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);
            partner.IsActive = false;

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, null);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerSetLimit_ShouldbyPromocodeLimit0()
        {
            // Arrange
            var partner = PartnerBuilder.CreateBasePartner().AddLimit(50);
            partner.NumberIssuedPromoCodes = 10;
            var limit = new SetPartnerPromoCodeLimitRequest { Limit = 5 };
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);
            int expected = 0;

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, limit);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(expected);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetOffPreviousLimit_ShouldSetCancelDateNow()
        {
            // Arrange
            var partner = PartnerBuilder.CreateBasePartner().AddLimit(50);
            DateTime now = new DateTime(2020, 11, 04);
            
            partner.NumberIssuedPromoCodes = 10;
            var limit = new SetPartnerPromoCodeLimitRequest { Limit = 5 };
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            _currentDateTimeProviderMock.Setup(x => x.CurrentDateTime)
                .Returns(now);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, limit);

            // Assert
            partner.PartnerLimits.FirstOrDefault().CancelDate.Should().Be(now);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerCountLimit0_ReturnsBadRequest()
        {
            // Arrange
            var partner = PartnerBuilder.CreateBasePartner().AddLimit(50);
            partner.NumberIssuedPromoCodes = 10;
            var limit = new SetPartnerPromoCodeLimitRequest { Limit = 0 };
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, limit);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_AddNewLimit_ReturnsOkObjectResult()
        {
            // Arrange
            var partner = PartnerBuilder.CreateBasePartner().AddLimit(50);
            var extected = partner.PartnerLimits.Count + 1;
            partner.NumberIssuedPromoCodes = 10;
            var limit = new SetPartnerPromoCodeLimitRequest { Limit = 10 };
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, limit);

            // Assert
            partner.PartnerLimits.Count.Should().Be(extected);
        }
    }
}