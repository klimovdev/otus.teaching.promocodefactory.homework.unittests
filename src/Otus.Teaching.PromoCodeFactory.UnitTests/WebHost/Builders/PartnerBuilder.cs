﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Builders
{
    public static class PartnerBuilder
    {
        public static Partner CreateBasePartner()
        {
            return new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
            };
        }

        public static Partner AddLimit(this Partner partner, int countLimit)
        {
            partner.PartnerLimits = new List<PartnerPromoCodeLimit>();
            partner.PartnerLimits.Add(new PartnerPromoCodeLimit()
            {
                Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                CreateDate = new DateTime(2020, 07, 9),
                EndDate = new DateTime(2020, 10, 9),
                Limit = countLimit,
                PartnerId = partner.Id,
                Partner = partner
            });

            return partner;
        }

    }
}
